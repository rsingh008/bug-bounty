/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.views

import io.cordite.Styles
import io.cordite.viewmodel.*
import tornadofx.*

class TransferTokensView : View("Transfer Tokens") {
  val ledgerViewModel: LedgerViewModel by inject()
  val transferViewModel: TransferViewModel by inject()

  override val root = borderpane {
    addClass(Styles.mainScreen)
    top {
      stackpane {
        label(title).addClass(Styles.heading)
      }
    }

    center {
      form {
        fieldset("Transfer Tokens") {

          field("From Account") {
            combobox(transferViewModel.fromAccount, ledgerViewModel.accounts) {
              cellFormat {
                text = it.name
              }
            }
          }

          field("To Account") {
            textfield(transferViewModel.toAccountUri)
          }

          field("Token") {
            combobox(transferViewModel.descriptor, ledgerViewModel.tokens) {
              cellFormat {
                text = it.symbol
              }
            }
          }

          field("Amount") {
            textfield(transferViewModel.amount)
          }

          field("Description") {
            textfield(transferViewModel.description)
          }

        }
      }.addClass(Styles.forms)
    }

    bottom {
      stackpane {
        addClass(Styles.content)
        button("transfer tokens") {
          enableWhen(transferViewModel.dirty)
          action {
            transferViewModel.commit {
              runAsync {
                ledgerViewModel.transferTokens(transferViewModel.item!!)
              }
            }
            close()
          }
        }
      }
    }
  }
}