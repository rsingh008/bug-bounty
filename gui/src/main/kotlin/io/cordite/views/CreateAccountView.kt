/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.views

import io.cordite.Styles
import io.cordite.viewmodel.*
import tornadofx.*

class CreateAccountView : View("Create Account") {
  val ledgerViewModel: LedgerViewModel by inject()
  val createAccountViewModel: CreateAccountViewModel by inject()

  override val root = borderpane {
    addClass(Styles.mainScreen)
    top {
      stackpane {
        label(title).addClass(Styles.heading)
      }
    }

    center {
      form {
        fieldset("Account") {
          field("Name") {
            textfield(createAccountViewModel.name)
          }
        }
      }.addClass(Styles.forms)
    }

    bottom {
      stackpane {
        addClass(Styles.content)
        button("create account") {
          enableWhen(createAccountViewModel.dirty)
          action {
            createAccountViewModel.commit {
              runAsync {
                ledgerViewModel.createAccount(createAccountViewModel.item)
              }
            }
            close()
          }
        }
      }
    }
  }
}