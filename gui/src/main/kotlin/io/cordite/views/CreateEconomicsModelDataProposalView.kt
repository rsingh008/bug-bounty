/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.views

import io.cordite.Styles
import io.cordite.viewmodel.*
import javafx.util.converter.IntegerStringConverter
import tornadofx.*

class CreateEconomicsModelDataProposalView : View("Create Economics Model Data Proposal") {
  val mutualViewModel: MutualViewModel by inject()
  val proposalViewModel: ProposalViewModel by inject()
  val economicsModelDataProposalModel: EconomicsModelDataModel by inject()

  override val root = borderpane {
    addClass(Styles.mainScreen)
    top {
      stackpane {
        label(title).addClass(Styles.heading)
      }
    }

    center {
      form {
        fieldset("Economics Model Data Proposal") {
          field("Proposal Name") {
            textfield(economicsModelDataProposalModel.name)
          }

          field("Token Name") {
            textfield(economicsModelDataProposalModel.tokenName)
          }

          field("Exponent") {
            textfield(economicsModelDataProposalModel.exponent, IntegerStringConverter())
          }

          field("Digital Mutual") {
            combobox(economicsModelDataProposalModel.mutual, mutualViewModel.mutualModels) {
              cellFormat {
                text = it.name
              }
            }
          }
        }
      }.addClass(Styles.forms)
    }

    bottom {
      stackpane {
        addClass(Styles.content)
        button("create proposal") {
          enableWhen(economicsModelDataProposalModel.dirty)
          action {
            economicsModelDataProposalModel.commit {
              runAsync {
                proposalViewModel.createEconomicsModelDataProposal(economicsModelDataProposalModel.item)
              }
            }
            close()
          }
        }
      }
    }
  }
}