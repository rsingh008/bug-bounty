/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.views

import io.cordite.Styles
import io.cordite.viewmodel.MutualViewModel
import io.cordite.viewmodel.NewMutualModel
import javafx.util.converter.IntegerStringConverter
import tornadofx.*

class CreateMutualView : View("Create Digital Mutual") {
  val mutualViewModel: MutualViewModel by inject()
  val newMutualModel: NewMutualModel by inject()

  override val root = borderpane {
    addClass(Styles.mainScreen)
    top {
      stackpane {
        label(title).addClass(Styles.heading)
      }
    }

    center {
      form {
        fieldset("New Mutual") {
          field("Name") {
            textfield(newMutualModel.name)
          }

          field("Min Members") {
            textfield(newMutualModel.minimumMemberCount, IntegerStringConverter())
          }

          field("Strict Mode") {
            checkbox("", newMutualModel.strictMode)
          }
        }
      }.addClass(Styles.forms)
    }

    bottom {
      stackpane {
        addClass(Styles.content)
        button("create dm") {
          enableWhen(newMutualModel.dirty)
          action {
            newMutualModel.commit {
              runAsync {
                mutualViewModel.createMutual(newMutualModel.item)
              }
            }
            close()
          }
        }
      }
    }
  }
}